﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DocumentStore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly IMetadataProvider metadataProvider;
        private readonly IStorageProvider storageProvider;

        public DocumentsController(IMetadataProvider metadataProvider, IStorageProvider storageProvider)
        {
            this.metadataProvider = metadataProvider;
            this.storageProvider = storageProvider;
        }
        
        // GET api/documents/person/67557259
        [HttpGet("{itemType}/{itemId}")]
        public ActionResult<List<DocumentInfo>> GetDocumentInfos(string itemType, string itemId)
        {
            return metadataProvider.GetDocumentInfos(itemType, itemId);
        }

        // PUT api/documents/workorder/544679
        [HttpPut("{itemType}/{itemId}/{filename}")]
        [DisableFormValueModelBinding]
        public async Task UploadDocument(string itemType, string itemId, string filename)
        {
            storageProvider.SaveDocument(itemType, itemId, filename, Request.Body);
            metadataProvider.AddDocumentInfo(itemType, itemId, Request.ContentType, filename);
            Response.StatusCode = 201;
            Response.Headers.Add("Content-Location", $"/{itemType}/{itemId}/{filename}" );
        }

        [HttpGet("{itemType}/{itemId}/{filename}")]
        [DisableFormValueModelBinding]
        public async Task GetDocument(string itemType, string itemId, string filename)
        {
            var documentInfo = metadataProvider.GetDocumentInfos(itemType, itemId).FirstOrDefault(di => di.FileName == filename);
            if (documentInfo == null)
            {
                Response.StatusCode = 404;
                return;
            }
            using (var dataStream = storageProvider.GetDocument(itemType, itemId, filename))
            {
                if (dataStream == null)
                {
                    Response.StatusCode = 404;
                    return;
                }
                Response.StatusCode = 200;
                Response.ContentType = documentInfo.ContentType;
                await dataStream.CopyToAsync(Response.Body);
            }
        }

        [HttpDelete("{itemType}/{itemId}/{filename}")]
        public void Delete(string itemType, string itemId, string filename)
        {
            metadataProvider.GetDocumentInfos(itemType, itemId).RemoveAll(di => di.FileName == filename);
            storageProvider.DeleteFile(itemType, itemId, filename);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var factories = context.ValueProviderFactories;
            factories.RemoveType<FormValueProviderFactory>();
            factories.RemoveType<JQueryFormValueProviderFactory>();
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
    }
}
