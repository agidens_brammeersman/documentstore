﻿using System.IO;
using System.Linq;

namespace DocumentStore
{
    public class StorageProvider : IStorageProvider
    {
        private readonly DirectoryInfo storageDir;

        public StorageProvider(Settings settings)
        {
            var documentsDir = settings.DocumentsDir;
            var currentDir = new DirectoryInfo(Directory.GetCurrentDirectory());
            storageDir = currentDir.GetDirectories(documentsDir).FirstOrDefault() ?? currentDir.CreateSubdirectory(documentsDir);
        }

        public async void SaveDocument(string itemType, string itemId, string filename, Stream requestBody)
        {
            var dir = GetDirectory(itemType, itemId, storageDir);

            using (var targetStream = File.Create(Path.Combine(dir.FullName, filename)))
            {
                await requestBody.CopyToAsync(targetStream);
            }
        }

        public void DeleteFile(string itemType, string itemId, string filename)
        {
            var dir = GetDirectory(itemType, itemId, storageDir);
            File.Delete(Path.Combine(dir.FullName, filename));
        }

        public FileStream GetDocument(string itemType, string itemId, string filename)
        {
            var dir = GetDirectory(itemType, itemId, storageDir);
            var filePath = Path.Combine(dir.FullName, filename);
            var sourceStream = File.OpenRead(filePath);
            return sourceStream;
        }

        private static DirectoryInfo GetDirectory(string itemtype, string itemId, DirectoryInfo baseDir)
        {
            var itemTypeDir = baseDir.GetDirectories(itemtype).FirstOrDefault() ?? baseDir.CreateSubdirectory(itemtype);
            var itemDir = itemTypeDir.GetDirectories(itemId).FirstOrDefault() ?? itemTypeDir.CreateSubdirectory(itemId);
            return itemDir;
        }
    }
}
