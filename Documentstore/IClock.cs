using System;

namespace DocumentStore
{
    public interface IClock
    {
        DateTime CurrentTime { get; }
    }

    public class Clock : IClock
    {
        public DateTime CurrentTime => DateTime.UtcNow;
    }
}