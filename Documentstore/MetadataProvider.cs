﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace DocumentStore
{
    public class MetadataProvider : IMetadataProvider
    {
        private readonly Dictionary<string, List<DocumentInfo>> documents;
        private readonly IClock clock;
        private readonly string metadataFile;
        private readonly string documentsDir;

        public MetadataProvider(Settings settings)
        {
            clock = new Clock();
            metadataFile = "metadata.json";
            documentsDir = settings.DocumentsDir;
            documents = LoadMetaData(documentsDir + "/" + metadataFile);
        }

        private void LoadTestMetadata()
        {
            var application_pdf = "application/pdf";
            var image_tiff = "image/tiff";

            AddDocumentInfo("person", "98563247", application_pdf, "scan_driverLicense.pdf");

            AddDocumentInfo("person", "67557259", application_pdf, "scan_driverLicense.pdf");
            AddDocumentInfo("person", "67557259", application_pdf, "scan_ADRCertificate.pdf");

            AddDocumentInfo("person", "54587223", application_pdf, "scan_driverLicense.pdf");

            AddDocumentInfo("workorder", "3344", image_tiff, "scan_CleaningCertificate.tiff");
        }

        private static Dictionary<string, List<DocumentInfo>> LoadMetaData(string metadatafile)
        {
            if (!File.Exists(metadatafile)) return new Dictionary<string, List<DocumentInfo>>();
            var serializer = new JsonSerializer();
            using (var streamReader = new StreamReader(metadatafile))
            using (var reader = new JsonTextReader(streamReader))
            {
                var metadata = serializer.Deserialize<Dictionary<string, List<DocumentInfo>>>(reader);
                return metadata;
            }
        }

        private static void SaveMetaData(string metadatafile, Dictionary<string, List<DocumentInfo>> metadata)
        {
            var serializer = new JsonSerializer();
            using (var streamWriter = new StreamWriter(metadatafile))
            using (var writer = new JsonTextWriter(streamWriter){Formatting = Formatting.Indented})
            {
                serializer.Serialize(writer, metadata);
            }
        }

        public void AddDocumentInfo(string itemType, string itemId, string mimetype, string filename)
        {
            var key = GetKey(itemType, itemId);
            if (!documents.ContainsKey(key) || documents[key] == null)
            {
                documents[key] = new List<DocumentInfo>();
            }
            var metadata = new DocumentInfo
            {
                FileName = filename,
                ContentType = mimetype,
                Href = $"/{documentsDir}/{itemType}/{itemId}/{filename}",
                UploadedWhen = clock.CurrentTime,
            };
            documents[key].RemoveAll(di => di.FileName == metadata.FileName);
            documents[key].Add(metadata);
            SaveMetaData(documentsDir + "/" + metadataFile, documents);
        }


        public List<DocumentInfo> GetDocumentInfos(string itemType, string itemId)
        {
            var key = GetKey(itemType, itemId);
            if (documents.ContainsKey(key) && documents[key] != null) 
                return documents[key];
            else
                return new List<DocumentInfo>();
        }

        private static string GetKey(string itemType, string itemId)
        {
            var key = $"{itemType}/{itemId}";
            return key;
        }
    }
}
