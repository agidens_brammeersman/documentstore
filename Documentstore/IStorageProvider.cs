﻿using System.IO;

namespace DocumentStore
{
    public interface IStorageProvider
    {
        void SaveDocument(string itemType, string itemId, string filename, Stream requestBody);
        void DeleteFile(string itemType, string itemId, string filename);
        FileStream GetDocument(string itemType, string itemId, string filename);
    }
}