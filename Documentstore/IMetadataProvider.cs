using System;
using System.Collections.Generic;

namespace DocumentStore
{
    public interface IMetadataProvider
    {
        List<DocumentInfo> GetDocumentInfos(string itemType, string itemId);
        void AddDocumentInfo(string itemType, string itemId, string mimetype, string filename);
    }

    public class DocumentInfo
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Href { get; set; }
        public DateTime UploadedWhen { get; set; }
    }
}